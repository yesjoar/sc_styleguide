/**
 *
 * set some variables
 *
 */

var gulp = require('gulp'),
	gutil = require('gulp-util'),
	uglify = require('gulp-uglify'),
	sass = require('gulp-sass'),
	autoprefixer = require('gulp-autoprefixer'),
	minifycss = require('gulp-clean-css'),
	browserSync = require('browser-sync');


gulp.task('sass', function(){
	gulp.src(['private/sass/sc_styleguide.scss'])
		.pipe(sass())
		.on('error', gutil.log)
		.pipe(autoprefixer({
			browser: ['last 2 version']
		}))
		.on('error', gutil.log)
		.pipe(minifycss())
		.pipe(gulp.dest('public/css/'))
		.pipe(browserSync.reload({stream:true}));
});



gulp.task('js', function(){
	gulp.src('private/js/sc_styleguide.js')
		.pipe(uglify())
		.on('error', gutil.log)
		.pipe(gulp.dest('public/js/'))
});




gulp.task('browser-sync', function() {
	browserSync({
		notify: false,
		server: {
			baseDir: './',
			index: 'index.html'
		}
	});
});


gulp.task('watch', function () {
	gulp.watch('private/sass/sc_styleguide.scss', ['sass']);
	gulp.watch('private/js/sc_styleguide.js', ['js']);
});

gulp.task('default', function(){
	gulp.start('sass', 'js', 'browser-sync', 'watch');
});
