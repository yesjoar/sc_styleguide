/**
 * bra_styleguide-widget.js v1.0.0
 * https://github.com/brandung/bra_styleguide-widget.git
 *
 * This widget generates an online style guide
 * within the Capitan workflow
 *
 * @author: Simon Kemmerling
 *
 * Copyright 2016, brandung GmbH & Co. KG
 * http://www.brandung.de
 *
 * MIT License (MIT)
 */

(function ($) {

	var self = {
			settings: {
			},
			classes: {
				el: 'sc-sg',
				headline: 'sc-sg__headline',
				content: 'sc-sg__content'
			},
			tpl: {
			}
		},
		_ ={};


	/**
	 * init the plugin
	 *
	 * @param {object} settings
	 */
	self.init = function (settings) {
		_.addDom();
	};


	_.addDom = function(){

		$('[data-role="' + self.classes.el + '"]').each(function(){

			var $this = $(this),
				type = $this.attr('data-type');

			$(this).wrapInner('<div class="' + self.classes.content + '" />');

			$this.prepend('<h2>' + $this.attr('data-title') + '</h2>');


		});
	};


	return self;

})(jQuery).init();